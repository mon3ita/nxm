# NxM

Some basic matrix manipulations implemented in `prolog`.

## Methods

### Constructors
create_matrix_nxn
create_matrix_nxn_zeros
create_matrix_nxn_ones
create_matrix_nxn_n
create_matrix_nxm
create_matrix_nxm_zeros
create_matrix_nxm_ones
create_matrix_nxm_n
create_matrix_nxm_range
identity

### Useful methods
row
col
element
shape

### Operations
multiply_matrices
multiply_by_n
sum
substr
transpose