%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PUBLIC %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Constructors.
create_matrix_nxn(M, N) :- create_matrix_(M, N, N, 0).
create_matrix_nxn_zeros(M, N) :- create_matrix_(M, N, N, 0).
create_matrix_nxn_ones(M, N) :- create_matrix_(M, N, N, 1).
create_matrix_nxn_n(M, N, VALUE) :- create_matrix_(M, N, N, VALUE).

create_matrix_nxm(M, ROWS, COLS) :- create_matrix_(M, ROWS, COLS, 0).
create_matrix_nxm_zeros(M, ROWS, COLS) :- create_matrix_(M, ROWS, COLS, 0).
create_matrix_nxm_ones(M, ROWS, COLS) :- create_matrix_(M, ROWS, COLS, 1).
create_matrix_nxm_n(M, ROWS, COLS, VAL) :- create_matrix_(M, ROWS, COLS, VAL).
create_matrix_nxm_range(M, ROWS, FROM, TO) :- create_matrix_range_(M, ROWS, FROM, TO).
identity(M, ROWS, COLS) :- create_matrix_nxm_zeros(MO, ROWS, COLS), set_id_(MO, -1, M, 1).

% Useful methods
row(M, ROW, N) :- nth(M, ROW, N).
col(M, N, RES) :- length(M, L), nth_col(M, RES, L, N).
element(M, ROW, COL, EL) :- nth(M, RES, ROW), nth(RES, EL, COL).
shape(M, ROWS, COLS) :- length(M, ROWS), nth(M, C, 0), length(C, COLS).

% Operations
multiply_matrices(M1, M2, MR) :- shape(M1, _, M1_COLS), shape(M2, M2_ROWS, _), M1_COLS == M2_ROWS,
                        mult_matrix_(M1, M2, MR), !.
multiply_by_n(M1, N, MR) :- mult_by_n(M1, N, MR).
sum(M1, M2, MR) :- shape(M1, M1_ROWS, M1_COLS), shape(M2, M2_ROWS, M2_COLS), M1_ROWS == M2_ROWS, M1_COLS == M2_COLS,
                        sum_matr_(M1, M2, MR).

substr(M1, M2, MR) :- shape(M1, M1_ROWS, M1_COLS), shape(M2, M2_ROWS, M2_COLS), M1_ROWS == M2_ROWS, M1_COLS == M2_COLS,
                        subs_matr_(M1, M2, MR).
transpose(M, T) :- shape(M, ROWS, COLS), transpose_(M, T, ROWS, COLS, 0).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% PRIVATE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Help methods
add_row_([], SIZE, _) :- SIZE == 0, !.
add_row_([VALUE|T], SIZE, VALUE) :- NEW_SIZE is SIZE - 1, add_row_(T, NEW_SIZE, VALUE).

add_row_range_([], FROM, TO) :- NEW_TO is TO + 1, FROM == NEW_TO, !.
add_row_range_([FROM|T], FROM, TO) :- NEW_FROM is FROM + 1, add_row_range_(T, NEW_FROM, TO).

create_matrix_([], ROWS, _, _) :- ROWS == 0, !.
create_matrix_([N|T], ROWS, COLS, FILL) :- NEW_ROWS is ROWS - 1, NEW_COLS is COLS, add_row_(N, NEW_COLS, FILL), create_matrix_(T, NEW_ROWS, COLS, FILL).

create_matrix_range_([], ROWS, _, _) :- ROWS == 0, !.
create_matrix_range_([R|T], ROWS, FROM, TO) :- NEW_ROWS is ROWS - 1, add_row_range_(R, FROM, TO), create_matrix_range_(T, NEW_ROWS, FROM, TO).

nth([H|_], H, N) :- N == 0, !.
nth([_|T], RESULT, N) :- NEW_N is N - 1, nth(T, RESULT, NEW_N).

nth_col([], [], ROWS, _) :- ROWS == 0, !.
nth_col([H|T], [NTH_EL|T1], ROWS, N) :- nth(H, NTH_EL, N), NEW_ROWS is ROWS - 1, nth_col(T, T1, NEW_ROWS, N).

transpose_(_, [], _, COLS, N) :- COLS == N, !.
transpose_(M, [H|T1], ROWS, COLS, N) :- nth_col(M, H, ROWS, N), NEW_N is N + 1, transpose_(M, T1, ROWS, COLS, NEW_N).

mult_matrix_([], _, []).
mult_matrix_([H1|T1], M2, [H|T]) :- transpose(M2, MT), multiply_row_cols_(H1, MT, H), mult_matrix_(T1, M2, T).

multiply_row_cols_(_, [], []).
multiply_row_cols_(R, [H|M2], [ROW|T]) :- mult(R, H, MULT), sum_(MULT, ROW), multiply_row_cols_(R, M2, T).

mult([], [], []).
mult([H1|T1], [H2|T2], [R|T]) :- R is H1 * H2, mult(T1, T2, T).

sum_([], 0).
sum_([H|T], NEW_SUM) :- sum_(T, SUM), NEW_SUM is SUM + H.

mult_by_n([], _, []).
mult_by_n([H|T], VAL, [RES|T2]) :- mult_row_(H, VAL, RES), mult_by_n(T, VAL,  T2).

mult_row_([], _, []).
mult_row_([H|T], VAL, [RES|T2]) :- RES is H * VAL, mult_row_(T, VAL, T2).

sum_matr_([], [], []).
sum_matr_([H1|T1], [H2|T2], [RES|TR]) :- sum_rows_(H1, H2, RES), sum_matr_(T1, T2, TR).

sum_rows_([], [], []).
sum_rows_([H|T], [H2|T2], [RES|TR]) :- RES is H + H2, sum_rows_(T, T2, TR).

subs_matr_([], [], []).
subs_matr_([H1|T1], [H2|T2], [RES|TR]) :- subs_rows_(H1, H2, RES), subs_matr_(T1, T2, TR).

subs_rows_([], [], []).
subs_rows_([H|T], [H2|T2], [RES|TR]) :- RES is H - H2, sum_rows_(T, T2, TR).

set_val_at_(T, I, I2, VAL, [VAL|T]) :- I == I2, !.
set_val_at_([H|T], I, I2, VAL, [H|TR]) :- NEW_I is I2 + 1, set_val_at_(T, I, NEW_I, VAL, TR).

set_id_([], _, [], _).
set_id_([H|T], I, [HR|TR], VAL) :- NEW_I is I + 1, set_val_at_(H, NEW_I, 0, VAL, HR), set_id_(T, NEW_I, TR, VAL).